# LabelBasic

## Description
LabelBasic - A simple quick basic interpreter that uses line labels instead of line numbers
and allows named variables.

Because labels are used you will never need to (or be able to) renumber your basic program
Variables will no longer be obtuse A1,  B7(x), C$  and can have meaningful names like SUM, Bottles, Cows.

## Visuals
LabelBasic is a CLI scripting language. It runs in a terminal emulator window, but If your program only uses file I/O then it can be launched from your favorite file manager (the LabelBasic interpreter understands the #! and any line beginning with '#' is a full line comment)

## Installation
LabelBasic is a standard C program and can be compiled with the supplied makefile. After that the resulting program can be copied anywhere in the path. Providing source is an important feature as you can extent LabelBasic easily to have aditional statments.

## Usage
1) Use the #! /path/to/LabelBasic and then simply mention the filename on the command line
 Note: If you have the #! as the first line of your program you may also double click it in your favorite file mananger or create a desktop file to run it from your launcher.
  
  or

2) LabelBasic nameofprogram.bas  
to execute the program from the CLI 

## Support
I am not sure what will be done for support at this time as I am only one guy. But hopefully there will be a population of happy LabelBasic users someday so perhaps there will be help available in some forum,~~subreddit~~Lemmy forum, discord location etc.


## Roadmap
It is very early in the process but most of the bases are covered by the initial design.

## Contributing
Help is alway appreciated. I am hoping there will be a core of programmers to help me actually code this as my C skills are Ok but not as professional as the could be.

## Authors and acknowledgment
The inital author and architecht is me, Ronald Hudson - hudson.ra@live.com 
As I said I hope to find others along the way

## License
LabelBasic will be licensed under GPL V3 or any later version of the GPL.

## Project status
LabelBasic is currently in the Definition / Design phase of the project. Watch this space for updates, and do read the "designdocument.txt"

